package com.ing.interview.util.examples;

import com.ing.interview.domain.command.car.CarCreateCommand;
import com.ing.interview.domain.model.Car;
import com.ing.interview.domain.model.CarCreateRequest;

//Car object mother
public class CarExamples {

    public static CarCreateRequest validCarDataColorBlueAndModelPeugeot(){
        return CarCreateRequest.builder()
                .age(5).color("BLUE").model("PEUGEOT")
                .build();
    }

    public static Car validCarDataColorBlueAndModelPeugeotCar(){
        return Car.builder()
                .age(5).color("BLUE").model("PEUGEOT")
                .build();
    }

    public static CarCreateCommand validCarDataColorBlueAndModelPeugeotCommand(){
        return CarCreateCommand.builder()
                .age(5).color("BLUE").model("PEUGEOT")
                .build();
    }

    public static CarCreateRequest carWithoutColorValidData(){
        return CarCreateRequest.builder()
                .age(5).model("PEUGEOT")
                .build();
    }

    public static CarCreateRequest carWithFiftyAge(){
        return CarCreateRequest.builder()
                .age(50)
                .model("PEUGEOT")
                .color("BLUE")
                .build();
    }

    public static CarCreateRequest carNotExistsModel(){
        return CarCreateRequest.builder()
                .age(1)
                .model("X243SDASD")
                .color("BLUE")
                .build();
    }

    public static CarCreateRequest carNotExistColor(){
        return CarCreateRequest.builder()
                .age(0)
                .model("FIAT")
                .color("MAGENTA")
                .build();
    }
}

package com.ing.interview.util.examples;

import com.ing.interview.domain.model.OrderStatus;

import java.time.LocalDateTime;

//OrderStatus Object mother
public class OrderStatusExamples {
    public static OrderStatus statusProcessing(){
        return OrderStatus.builder()
                .assignedTo("Sergi")
                .stage("processing")
                .lastUpdate(LocalDateTime.now())
                .build();
    }

    public static OrderStatus statusPending(){
        return OrderStatus.builder()
                .assignedTo("Tomas")
                .stage("pending")
                .lastUpdate(LocalDateTime.now())
                .build();
    }
}

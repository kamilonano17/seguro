package com.ing.interview.infrastructure.adapter.in.rest.car;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ing.interview.InterviewApplication;
import com.ing.interview.util.examples.CarExamples;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = InterviewApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CarCreateControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void givenCarWhenCreateThenReturnIsCreated() throws Exception {
        // given: Request Car
        ObjectMapper mapper = new ObjectMapper();
        String carContent = mapper.writeValueAsString(CarExamples.validCarDataColorBlueAndModelPeugeot());

        this.mockMvc
            .perform(
                post("/car")
                    .contentType(APPLICATION_JSON)
                    .content(carContent))
            .andDo(print())
            // Response Status
            .andExpect(status().isCreated())
            .andExpect(content().contentType(APPLICATION_JSON))
            // Response Body
            .andExpect(jsonPath("$.model").value("PEUGEOT"))
            .andExpect(jsonPath("$.orderDate").exists());
    }

    @Test
    void givenCarWithoutColorWhenCreateThenReturnIsCreated() throws Exception {
        // given: Request Car
        ObjectMapper mapper = new ObjectMapper();
        String carContent = mapper.writeValueAsString(CarExamples.carWithoutColorValidData());

        this.mockMvc
                .perform(
                        post("/car")
                                .contentType(APPLICATION_JSON)
                                .content(carContent))
                .andDo(print())
                // Response Status
                .andExpect(status().isCreated())
                .andExpect(content().contentType(APPLICATION_JSON))
                // Response Body
                .andExpect(jsonPath("$.model").value("PEUGEOT"))
                .andExpect(jsonPath("$.orderDate").exists())
                .andExpect(jsonPath("$.color").value("BLUE"));
    }

    @Test
    void givenCarWhenNotExistsModelThenReturnIsNoContent() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String carContent = mapper.writeValueAsString(CarExamples.carNotExistsModel());
        this.mockMvc
                .perform(
                        post("/car")
                                .contentType(APPLICATION_JSON)
                                .content(carContent))
                .andDo(print())
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    void givenCarWhenUninsurableCarThenReturnIsBadRequest() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String carContent = mapper.writeValueAsString(CarExamples.carWithFiftyAge());
        this.mockMvc
                .perform(
                        post("/car")
                                .contentType(APPLICATION_JSON)
                                .content(carContent))
                .andDo(print())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message").value("Uninsurable car. model:PEUGEOT, age: 50"));
    }

}
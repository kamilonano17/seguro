package com.ing.interview.infrastructure.adapter.in.rest.order;

import com.ing.interview.InterviewApplication;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = InterviewApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GetOrderControllerIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void givenStatusProcessingThenReturnIsOk() throws Exception {
        this.mockMvc
                .perform(
                        get("/order/2/status")
                                .contentType(APPLICATION_JSON)
                                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.assignedTo").value("Sergi"))
                .andExpect(jsonPath("$.stage").value("processing"))
                .andExpect(jsonPath("$.lastUpdate").exists());
    }

    @Test
    void givenStatusPendingThenReturnIsOk() throws Exception {
        this.mockMvc
                .perform(
                        get("/order/1/status")
                                .contentType(APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("$.assignedTo").value("Tomas"))
                .andExpect(jsonPath("$.stage").value("pending"))
                .andExpect(jsonPath("$.lastUpdate").exists());
    }
}
package com.ing.interview.application.service.car;

import com.ing.interview.domain.command.query.car.GetCarCommand;
import com.ing.interview.domain.dipatcher.DispatcherQueryCommand;
import com.ing.interview.domain.exception.CarNotAvailable;
import com.ing.interview.domain.mapper.MapperDomainImpl;
import com.ing.interview.domain.model.Car;
import com.ing.interview.infrastructure.adapter.out.sql.CarRepositoryConnector;
import com.ing.interview.util.examples.CarExamples;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ResolvableType;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GetCarServiceTest {

    @Test
    void testGetCarByIdExpectedCar() {
        String[] beans = {"beanQuery"};
        String uuid = UUID.randomUUID().toString();
        Car carExpected = CarExamples.validCarDataColorBlueAndModelPeugeotCar();
        carExpected.setId(uuid);

        ApplicationContext applicationContext = mock(ApplicationContext.class);
        DispatcherQueryCommand queryCommandDispatcher = mock(DispatcherQueryCommand.class);

        //when
        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(CarRepositoryConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(GetCarCommand.class))).thenReturn(Optional.of(carExpected));

        GetCarService getCarService = new GetCarService(queryCommandDispatcher,new MapperDomainImpl());
        Car carResponse = getCarService.getById(uuid);
        assertEquals(carExpected,carResponse);
    }

    @Test
    void testGetCarByIdExpectedThrowCarNotAvailable() {
        String[] beans = {"beanQuery"};
        String uuid = UUID.randomUUID().toString();
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        DispatcherQueryCommand queryCommandDispatcher = mock(DispatcherQueryCommand.class);
        //when
        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(CarRepositoryConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(GetCarCommand.class))).thenReturn(Optional.empty());

        //then
        GetCarService getCarService = new GetCarService(queryCommandDispatcher,new MapperDomainImpl());
        assertThrows(CarNotAvailable.class,()->{getCarService.getById(uuid);});
    }
}
package com.ing.interview.application.service.order;

import com.ing.interview.domain.command.query.order.GetOrderStatusCommad;
import com.ing.interview.domain.dipatcher.DispatcherQueryCommand;
import com.ing.interview.domain.mapper.MapperDomainImpl;
import com.ing.interview.domain.model.OrderStatus;
import com.ing.interview.infrastructure.adapter.out.sql.CarRepositoryConnector;
import com.ing.interview.util.examples.OrderStatusExamples;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ResolvableType;


import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GetOrderServiceTest {

    @Test
    void testOrderStatusServiceExpectedValues() {
        String[] beans = {"beanQuery"};
        OrderStatus expected = OrderStatusExamples.statusPending();
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        DispatcherQueryCommand queryCommandDispatcher = mock(DispatcherQueryCommand.class);
        //when
        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(CarRepositoryConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(GetOrderStatusCommad.class))).thenReturn(OrderStatusExamples.statusPending());

        //then
        GetOrderService getCarService = new GetOrderService(queryCommandDispatcher,new MapperDomainImpl());
        OrderStatus status = getCarService.getOrderStatus(1L);
        assertEquals(status.getAssignedTo(),expected.getAssignedTo());
        assertEquals(status.getStage(),expected.getStage());
    }

    @Test
    void testOrderStatusServiceNotExpectedValues() {
        String[] beans = {"beanQuery"};
        OrderStatus expected = OrderStatusExamples.statusPending();
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        DispatcherQueryCommand queryCommandDispatcher = mock(DispatcherQueryCommand.class);
        //when
        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(CarRepositoryConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(GetOrderStatusCommad.class))).thenReturn(null);

        //then
        GetOrderService getCarService = new GetOrderService(queryCommandDispatcher,new MapperDomainImpl());
        OrderStatus status = getCarService.getOrderStatus(1L);
        assertNull(status);
    }
}
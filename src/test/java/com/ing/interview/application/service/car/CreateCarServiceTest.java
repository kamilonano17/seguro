package com.ing.interview.application.service.car;

import com.ing.interview.domain.command.query.color.GetColorPickerCommand;
import com.ing.interview.domain.command.query.car.IsAvailableCarCommand;
import com.ing.interview.domain.command.query.car.ValidateInsuraceCarCommand;
import com.ing.interview.domain.dipatcher.DispatcherCommand;
import com.ing.interview.domain.dipatcher.DispatcherQueryCommand;
import com.ing.interview.domain.dipatcher.impl.DispatcherComandLocal;
import com.ing.interview.domain.exception.CarNotAvailable;
import com.ing.interview.domain.exception.UninsurableCar;
import com.ing.interview.domain.mapper.MapperDomainImpl;
import com.ing.interview.domain.model.Car;
import com.ing.interview.domain.model.CarCreateRequest;
import com.ing.interview.infrastructure.adapter.out.rest.CarAvailabilityRestConnector;
import com.ing.interview.infrastructure.adapter.out.rest.ColorPickerRestConnector;
import com.ing.interview.infrastructure.adapter.out.rest.InsuranceRestConnector;
import com.ing.interview.util.examples.CarExamples;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ResolvableType;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;


class CreateCarServiceTest {

    @Test
    void givenCommandWhenCreateThenExpectedValues() {

        // given
        CarCreateRequest carCreateCommand = CarExamples.validCarDataColorBlueAndModelPeugeot();

        String[] beans = {"beanPruebaQuery"};
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        DispatcherQueryCommand queryCommandDispatcher = mock(DispatcherQueryCommand.class);
        DispatcherCommand commandDispatcher = mock(DispatcherComandLocal.class);

        //when
        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(CarAvailabilityRestConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(IsAvailableCarCommand.class))).thenReturn(Boolean.TRUE);

        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(InsuranceRestConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(ValidateInsuraceCarCommand.class))).thenReturn(Boolean.TRUE);


        CreateCarService sut = new CreateCarService(commandDispatcher, queryCommandDispatcher, new MapperDomainImpl());
        final Car result = sut.create(carCreateCommand);

        // then
        assertEquals("BLUE", result.getColor());
        assertEquals("PEUGEOT", result.getModel());
    }

    @Test
    void givenCommandWhenCreateThenExpectedColorByDefault() {

        // given
        CarCreateRequest carCreateCommand = CarExamples.carWithoutColorValidData();

        String[] beans = {"beanPruebaQuery"};
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        DispatcherQueryCommand queryCommandDispatcher = mock(DispatcherQueryCommand.class);
        DispatcherCommand commandDispatcher = mock(DispatcherComandLocal.class);

        //when
        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(CarAvailabilityRestConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(IsAvailableCarCommand.class))).thenReturn(Boolean.TRUE);

        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(InsuranceRestConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(ValidateInsuraceCarCommand.class))).thenReturn(Boolean.TRUE);

        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(ColorPickerRestConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(GetColorPickerCommand.class))).thenReturn(Optional.of("RED"));

        CreateCarService sut = new CreateCarService(commandDispatcher, queryCommandDispatcher, new MapperDomainImpl());
        final Car result = sut.create(carCreateCommand);
        // then
        assertEquals("RED", result.getColor());
    }


    @Test
    void throwExceptionByCarNotAvialable() {

        // given
        CarCreateRequest carCreateCommand = CarExamples.carNotExistsModel();

        String[] beans = {"beanPruebaQuery"};
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        DispatcherQueryCommand queryCommandDispatcher = mock(DispatcherQueryCommand.class);
        DispatcherCommand commandDispatcher = mock(DispatcherComandLocal.class);

        //when
        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(CarAvailabilityRestConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(IsAvailableCarCommand.class))).thenReturn(Boolean.FALSE);

        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(InsuranceRestConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(ValidateInsuraceCarCommand.class))).thenReturn(Boolean.TRUE);

        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(ColorPickerRestConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(GetColorPickerCommand.class))).thenReturn(Optional.of("RED"));

        CreateCarService sut = new CreateCarService(commandDispatcher, queryCommandDispatcher, new MapperDomainImpl());
        // then
        assertThrows(CarNotAvailable.class,() ->{sut.create(carCreateCommand);}) ;

    }

    @Test
    void throwExceptionByUninsurableCar() {

        // given
        CarCreateRequest carCreateCommand = CarExamples.carNotExistsModel();

        String[] beans = {"beanPruebaQuery"};
        ApplicationContext applicationContext = mock(ApplicationContext.class);
        DispatcherQueryCommand queryCommandDispatcher = mock(DispatcherQueryCommand.class);
        DispatcherCommand commandDispatcher = mock(DispatcherComandLocal.class);

        //when
        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(CarAvailabilityRestConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(IsAvailableCarCommand.class))).thenReturn(Boolean.TRUE);

        when(applicationContext.getBeanNamesForType(any(ResolvableType.class))).thenReturn(beans);
        when(applicationContext.getBean(Mockito.anyString(), any(Class.class))).thenReturn(mock(InsuranceRestConnector.class));
        when(queryCommandDispatcher.sendAndAwait(any(ValidateInsuraceCarCommand.class))).thenReturn(Boolean.FALSE);

        // then
        CreateCarService sut = new CreateCarService(commandDispatcher, queryCommandDispatcher, new MapperDomainImpl());
        assertThrows(UninsurableCar.class,() ->{sut.create(carCreateCommand);}) ;

    }
}
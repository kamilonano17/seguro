package com.ing.interview.application.service.car;

import com.ing.interview.domain.dipatcher.DispatcherCommand;
import com.ing.interview.domain.dipatcher.DispatcherQueryCommand;

import com.ing.interview.domain.exception.CarNotAvailable;
import com.ing.interview.domain.exception.UninsurableCar;
import com.ing.interview.domain.mapper.MapperDomain;
import com.ing.interview.domain.model.Car;
import com.ing.interview.domain.model.CarCreateRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;
import java.util.UUID;


@Component
@AllArgsConstructor
public class CreateCarService {

    private DispatcherCommand dispatcherCommand;
    private DispatcherQueryCommand dispatcherQueryCommand;
    private MapperDomain mapperDomain;

    public Car create(final CarCreateRequest requestCar) {
        Car car = mapperDomain.toCar(requestCar,UUID.randomUUID().toString(), new Date());
        this.validateColor(car);
        this.validateAvailableCar(car);
        this.validateInsuranceCar(car);
        dispatcherCommand.send(mapperDomain.toCarCreateCommand(car), car.getId());
        return car;
    }

    private void validateColor(Car car) {
        if (car.getColor() == null || car.getColor().isEmpty()) {
            Optional<String> color = dispatcherQueryCommand.sendAndAwait(mapperDomain.toGetColorPickerCarCommand(car));
            car.setColor(color.orElse(null));
        }
    }

    private void validateAvailableCar(Car car) {
        Boolean isAvailableCar = dispatcherQueryCommand.sendAndAwait(mapperDomain.toIsAvailableCarCommand(car));
        if (!isAvailableCar) {
            throw new CarNotAvailable("Car not available, color: "+car.getColor()+", model: "+car.getModel());
        }
    }

    private void validateInsuranceCar(Car car) {
        Boolean isElegibleCar = dispatcherQueryCommand.sendAndAwait(mapperDomain.toValidateInsuraceCarCommand(car));
        if (!isElegibleCar) {
            throw new UninsurableCar("Uninsurable car. model:"+car.getModel()+", age: "+car.getAge());
        }
    }

}

package com.ing.interview.application.service.car;

import com.ing.interview.domain.command.query.car.GetCarCommand;
import com.ing.interview.domain.dipatcher.DispatcherQueryCommand;
import com.ing.interview.domain.exception.CarNotAvailable;
import com.ing.interview.domain.mapper.MapperDomain;
import com.ing.interview.domain.model.Car;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
public class GetCarService {
    private final DispatcherQueryCommand dispatcherQueryCommand;
    private final MapperDomain mapperDomain;

    public Car getById(String id){
        GetCarCommand getCarCommand = mapperDomain.toGetCarCommand(id);
        Optional<Car> car = dispatcherQueryCommand.sendAndAwait(getCarCommand);
        return car.orElseThrow(()-> new CarNotAvailable("Car not found. id: "+id));
    }
}

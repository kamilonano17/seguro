package com.ing.interview.application.service.order;

import com.ing.interview.domain.command.query.order.GetOrderStatusCommad;
import com.ing.interview.domain.dipatcher.DispatcherQueryCommand;
import com.ing.interview.domain.mapper.MapperDomain;
import com.ing.interview.domain.model.OrderStatus;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class GetOrderService {
    private DispatcherQueryCommand dispatcherQueryCommand;
    private MapperDomain mapperDomain;

    public OrderStatus getOrderStatus(Long id){
        GetOrderStatusCommad getOrderStatusCommad = mapperDomain.toGetOrderStatusCommad(id);
        OrderStatus staus = dispatcherQueryCommand.sendAndAwait(getOrderStatusCommad);
        return staus;
    }
}

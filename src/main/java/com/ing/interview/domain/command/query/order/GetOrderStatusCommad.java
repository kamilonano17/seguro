package com.ing.interview.domain.command.query.order;

import com.ing.interview.domain.dipatcher.CommandQuery;
import com.ing.interview.domain.model.OrderStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GetOrderStatusCommad extends CommandQuery<OrderStatus> {
    private Long id;
}

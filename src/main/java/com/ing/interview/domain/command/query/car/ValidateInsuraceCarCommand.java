package com.ing.interview.domain.command.query.car;

import com.ing.interview.domain.dipatcher.CommandQuery;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ValidateInsuraceCarCommand extends CommandQuery<Boolean>{
    private final Integer age;
    private final String model;
}

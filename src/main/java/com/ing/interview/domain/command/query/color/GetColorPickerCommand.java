package com.ing.interview.domain.command.query.color;

import com.ing.interview.domain.dipatcher.CommandQuery;
import lombok.Builder;
import lombok.Data;

import java.util.Optional;

@Data
@Builder
public class GetColorPickerCommand extends CommandQuery<Optional<String>> {
    private final String model;
}

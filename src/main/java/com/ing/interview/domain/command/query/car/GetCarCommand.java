package com.ing.interview.domain.command.query.car;

import com.ing.interview.domain.dipatcher.CommandQuery;
import com.ing.interview.domain.model.Car;
import lombok.Builder;
import lombok.Data;

import java.util.Optional;

@Data
@Builder
public class GetCarCommand extends CommandQuery<Optional<Car>> {
    private final String id;
}

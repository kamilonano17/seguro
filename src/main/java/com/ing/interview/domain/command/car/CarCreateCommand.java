package com.ing.interview.domain.command.car;

import com.ing.interview.domain.dipatcher.Command;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.Date;

@Data
@SuperBuilder
public class CarCreateCommand extends Command {
    private final String id;
    private final Integer age;
    private final String color;
    private final String model;
    private final Date orderDate;
}

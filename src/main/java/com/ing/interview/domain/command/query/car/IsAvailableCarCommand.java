package com.ing.interview.domain.command.query.car;

import com.ing.interview.domain.dipatcher.CommandQuery;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class IsAvailableCarCommand extends CommandQuery<Boolean> {
    private final String color;
    private final String model;
}

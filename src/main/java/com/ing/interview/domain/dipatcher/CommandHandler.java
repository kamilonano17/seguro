package com.ing.interview.domain.dipatcher;

public interface CommandHandler<T>  {
        void commandEvent(T event);
}

package com.ing.interview.domain.dipatcher.impl;

import com.ing.interview.domain.dipatcher.*;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationContext;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Service;

import java.lang.reflect.ParameterizedType;
import java.util.Arrays;
import java.util.Optional;

@Service
@AllArgsConstructor
public class DispatcherComandLocal implements DispatcherCommand, DispatcherQueryCommand {

    private final ApplicationContext applicationContext;

    public void send(final Command command, final String uuid){
            String[] namesBeansCommanQuery = applicationContext.getBeanNamesForType(ResolvableType.forClassWithGenerics(
                    CommandHandler.class,command.getClass()
            ));
            Arrays.stream(namesBeansCommanQuery)
                    .forEach( nameBean ->applicationContext.getBean(nameBean,CommandHandler.class).commandEvent(command));

    }

    public Object sendAndAwait(final CommandQuery command){
        ParameterizedType parameterizedType = null;
        parameterizedType = (ParameterizedType) command.getClass().getGenericSuperclass();
        Class<?> classReturnGeneric = null;
        if(parameterizedType.getActualTypeArguments()[0].getClass().getName().equalsIgnoreCase(
                "sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl")){
            classReturnGeneric = Optional.class;
        }else {
            parameterizedType = (ParameterizedType) command.getClass().getGenericSuperclass();
           classReturnGeneric = (Class<?>) parameterizedType.getActualTypeArguments()[0];
        }

        String[] namesBeansCommanQuery = applicationContext.getBeanNamesForType(ResolvableType.forClassWithGenerics(
                CommandQueryHandler.class,command.getClass(),classReturnGeneric));

        if(namesBeansCommanQuery.length <=0){
            throw new RuntimeException("Handler command not found for "+command.getClass().getName());
        }
        CommandQueryHandler handler = applicationContext.getBean(namesBeansCommanQuery[0],CommandQueryHandler.class);
        return handler.commanQueryEvent(command);
    }
}

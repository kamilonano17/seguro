package com.ing.interview.domain.dipatcher;

public interface DispatcherCommand {
    void send(Command command, String uuid);
}

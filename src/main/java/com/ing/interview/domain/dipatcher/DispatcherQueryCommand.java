package com.ing.interview.domain.dipatcher;

public interface DispatcherQueryCommand {
    <T> T sendAndAwait(CommandQuery command);
}

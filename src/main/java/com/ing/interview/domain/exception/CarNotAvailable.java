package com.ing.interview.domain.exception;

public class CarNotAvailable extends RuntimeException {

    public CarNotAvailable(String message) {
        super(message);
    }
}

package com.ing.interview.domain.exception;

public class UninsurableCar extends RuntimeException {

    public UninsurableCar(String message) {
        super(message);
    }
}

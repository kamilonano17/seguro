package com.ing.interview.domain.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
@Data
@Builder
public class OrderStatus {

    private final String assignedTo;

    private final LocalDateTime lastUpdate;

    private final String stage;
}

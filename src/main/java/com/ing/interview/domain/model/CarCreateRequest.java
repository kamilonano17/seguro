package com.ing.interview.domain.model;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;

@Data
@Builder
public class CarCreateRequest {
    @NonNull
    private final Integer age;

    private final String color;

    @NonNull
    @NotBlank
    private final String model;
}

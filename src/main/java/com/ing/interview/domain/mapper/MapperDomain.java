package com.ing.interview.domain.mapper;

import com.ing.interview.domain.command.car.CarCreateCommand;
import com.ing.interview.domain.command.query.car.GetCarCommand;
import com.ing.interview.domain.command.query.color.GetColorPickerCommand;
import com.ing.interview.domain.command.query.car.IsAvailableCarCommand;
import com.ing.interview.domain.command.query.car.ValidateInsuraceCarCommand;
import com.ing.interview.domain.command.query.order.GetOrderStatusCommad;
import com.ing.interview.domain.model.Car;
import com.ing.interview.domain.model.CarCreateRequest;
import com.ing.interview.infrastructure.config.MapStructConfig;
import org.mapstruct.Mapper;

import java.util.Date;


@Mapper(componentModel = "spring", config = MapStructConfig.class)
public abstract class MapperDomain {

    public abstract Car toCar(CarCreateRequest carCreateRequest, String id, Date orderDate);

    public abstract CarCreateCommand toCarCreateCommand(Car car);

    public abstract IsAvailableCarCommand toIsAvailableCarCommand(Car car);

    public abstract GetColorPickerCommand toGetColorPickerCarCommand(Car car);

    public abstract ValidateInsuraceCarCommand toValidateInsuraceCarCommand(Car car);

    public abstract GetCarCommand toGetCarCommand(String id);

    public abstract GetOrderStatusCommad toGetOrderStatusCommad(Long id);
}

package com.ing.interview.infrastructure.adapter.in.rest.car;


import com.ing.interview.application.service.car.CreateCarService;
import com.ing.interview.domain.model.Car;
import com.ing.interview.domain.model.CarCreateRequest;
import com.ing.interview.infrastructure.adapter.in.rest.car.doc.CarCreatControllerDoc;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/car")
@AllArgsConstructor
public class CarCreateController implements CarCreatControllerDoc {

    private final CreateCarService createCarService;

    @PostMapping
    public ResponseEntity<Car> create(@RequestBody @Validated CarCreateRequest carRequest) {
        Car car = createCarService.create(carRequest);
        return ResponseEntity.created(URI.create(String.format("/car/%s", car.getId()))).body(car);
    }
}

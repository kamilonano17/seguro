package com.ing.interview.infrastructure.adapter.out.rest;

import com.ing.interview.domain.command.query.color.GetColorPickerCommand;
import com.ing.interview.domain.dipatcher.CommandQueryHandler;
import com.ing.interview.infrastructure.adapter.out.rest.client.ColorPickerRestClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
public class ColorPickerRestConnector implements CommandQueryHandler<GetColorPickerCommand,Optional<String>> {

    private ColorPickerRestClient colorPickerRestClient;

    @Override
    public Optional<String> commanQueryEvent(GetColorPickerCommand command) {
        return colorPickerRestClient.pickColor(command.getModel());
    }
}

package com.ing.interview.infrastructure.adapter.in.rest.shared.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FailResponse {
    private String message;
}

package com.ing.interview.infrastructure.adapter.in.rest.shared.exception_handler;

import com.ing.interview.domain.exception.CarNotAvailable;
import com.ing.interview.domain.exception.UninsurableCar;
import com.ing.interview.infrastructure.adapter.in.rest.shared.model.FailResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
@RequestMapping(produces = "application/vnd.error+json")
@AllArgsConstructor
public class ExceptionHandlingController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CarNotAvailable.class)
    @ResponseBody
    public ResponseEntity<Object> carNotAvailable(CarNotAvailable ex, WebRequest request) {
        log.info(ex.getMessage());
        return new ResponseEntity(null, new HttpHeaders(),HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(UninsurableCar.class)
    @ResponseBody
    public ResponseEntity<Object> uninsurableCar(UninsurableCar ex, WebRequest request) {
        FailResponse error = FailResponse.builder()
                .message(ex.getMessage())
                .build();
        logger.info(ex.getMessage());
        return new ResponseEntity(error, new HttpHeaders(),HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<FailResponse> uncontrolledError(Exception ex) {
        FailResponse failResponse = FailResponse.builder()
                .message("Internal Error")
                .build();
        log.error("An error has ocurred",ex);
        return new ResponseEntity(failResponse, new HttpHeaders(),HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

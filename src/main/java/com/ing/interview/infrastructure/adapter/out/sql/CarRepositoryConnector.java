package com.ing.interview.infrastructure.adapter.out.sql;

import com.ing.interview.domain.command.car.CarCreateCommand;
import com.ing.interview.domain.command.query.car.GetCarCommand;
import com.ing.interview.domain.dipatcher.CommandHandler;
import com.ing.interview.domain.dipatcher.CommandQueryHandler;
import com.ing.interview.domain.model.Car;
import com.ing.interview.infrastructure.adapter.out.sql.entity.CarEntity;
import com.ing.interview.infrastructure.adapter.out.sql.repository.CarRepository;
import com.ing.interview.infrastructure.mapper.MapperAdapter;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
public class CarRepositoryConnector implements CommandHandler<CarCreateCommand>, CommandQueryHandler<GetCarCommand, Optional<Car>> {
    private CarRepository carRepository;
    private MapperAdapter mapperAdapter;


    @Override
    public void commandEvent(CarCreateCommand command) {
        carRepository.save(mapperAdapter.toCarEntity(command));
    }

    @Override
    public Optional<Car> commanQueryEvent(GetCarCommand event) {
        Optional<CarEntity> carEntityOpt = carRepository.findById(event.getId());
        return carEntityOpt.map(entity -> mapperAdapter.toCar(entity));
    }
}

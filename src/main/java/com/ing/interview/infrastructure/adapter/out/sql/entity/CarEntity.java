package com.ing.interview.infrastructure.adapter.out.sql.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarEntity {

    @Id
    @Column(name = "ID", nullable = false)
    private String id;

    private String color;

    private String model;

    private LocalDate orderDate;

}

package com.ing.interview.infrastructure.adapter.out.rest.client;

import com.ing.interview.domain.model.OrderStatus;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class OrderStatusRestClient {

    public OrderStatus checkOrderStatus(Long id) {
        return id % 2 == 0
            ? OrderStatus.builder().assignedTo("Sergi").stage("processing").lastUpdate(LocalDateTime.now()).build()
            : OrderStatus.builder().assignedTo("Tomas").stage("pending").lastUpdate(LocalDateTime.now()).build();
    }

}

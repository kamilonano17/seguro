package com.ing.interview.infrastructure.adapter.in.rest.order;

import com.ing.interview.application.service.order.GetOrderService;
import com.ing.interview.domain.model.OrderStatus;
import com.ing.interview.infrastructure.adapter.in.rest.order.doc.GetOrderControllerDoc;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/order")
public class GetOrderController implements GetOrderControllerDoc {
    private final GetOrderService getOrderService;

    @GetMapping("/{id}/status")
    public ResponseEntity<OrderStatus> getOrderStatus(@PathVariable("id") Long id){
        return ResponseEntity.ok(getOrderService.getOrderStatus(id));
    }
}

package com.ing.interview.infrastructure.adapter.out.rest;

import com.ing.interview.domain.command.query.car.ValidateInsuraceCarCommand;
import com.ing.interview.domain.dipatcher.CommandQueryHandler;
import com.ing.interview.infrastructure.adapter.out.rest.client.InsuranceRestClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class InsuranceRestConnector implements CommandQueryHandler<ValidateInsuraceCarCommand,Boolean> {

    private final InsuranceRestClient insuranceRestClient;

    @Override
    public Boolean commanQueryEvent(ValidateInsuraceCarCommand command) {
        return insuranceRestClient.isEligible(command.getAge(),command.getModel());
    }
}

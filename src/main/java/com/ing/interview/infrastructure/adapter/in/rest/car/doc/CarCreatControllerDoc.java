package com.ing.interview.infrastructure.adapter.in.rest.car.doc;

import com.ing.interview.domain.model.Car;
import com.ing.interview.domain.model.CarCreateRequest;
import com.ing.interview.infrastructure.adapter.in.rest.shared.model.FailResponse;
import io.swagger.annotations.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

@Api(tags = "Create Car")
public interface CarCreatControllerDoc {

    @ApiOperation(value = "Create order car", response = Car.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 400, response = FailResponse.class, message = "Uninsurable car.",
                    examples = @io.swagger.annotations.Example(
                            value = {
                                    @ExampleProperty(value = "{'message': 'Uninsurable car. model:FIAT, age: 50'}", mediaType = "application/json")
                            })),
            @ApiResponse(code = 201, response = Car.class, message = "Car create.",
                    examples = @io.swagger.annotations.Example(
                            value = {
                                    @ExampleProperty(value = "{\n" +
                                            "  'id': '09091a85-fd46-4b3d-9173-fe0da6b7424f',\n" +
                                            "  'color': 'YELLOW',\n" +
                                            "  'model': 'FIAT',\n" +
                                            "  'age': 1,\n" +
                                            "  'orderDate': '2021-10-24T21:23:22.681+00:00'\n" +
                                            "}", mediaType = "application/json")
                            })
            ),
            @ApiResponse(code = 204, message = "Car not avialable")
    })
    ResponseEntity<Car> create(@RequestBody @Validated CarCreateRequest carRequest);
}

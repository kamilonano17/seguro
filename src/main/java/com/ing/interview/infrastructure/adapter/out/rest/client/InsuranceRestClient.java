package com.ing.interview.infrastructure.adapter.out.rest.client;

import org.springframework.stereotype.Component;

import javax.print.DocFlavor;
import java.util.HashMap;
import java.util.Map;

@Component
public class InsuranceRestClient {

    private static final Map<String, Integer> KYC = new HashMap<String, Integer>() {{
        put("PEUGEOT", 18);
        put("FIAT", 20);
        put("MERCEDES", 50);
    }};

    public boolean isEligible(int age, String model) {
        if(!KYC.containsKey(model)){
            return false;
        }
        return KYC.get(model) >= age;
    }

}

package com.ing.interview.infrastructure.adapter.in.rest.order.doc;

import com.ing.interview.domain.model.Car;
import com.ing.interview.domain.model.OrderStatus;
import com.ing.interview.infrastructure.adapter.in.rest.shared.model.FailResponse;
import io.swagger.annotations.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

@Api(tags = "Api order status")
public interface GetOrderControllerDoc {
    @ApiOperation(value = "Get status order by id", response = OrderStatus.class, produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, response = Car.class, message = "Car create.",
                    examples = @io.swagger.annotations.Example(
                            value = {
                                    @ExampleProperty(value = "{\n" +
                                            "  \"assignedTo\": \"Tomas\",\n" +
                                            "  \"lastUpdate\": \"2021-10-24T16:37:16.5781266\",\n" +
                                            "  \"stage\": \"pending\"\n" +
                                            "}", mediaType = "application/json")
                            })
            )
    })
    ResponseEntity<OrderStatus> getOrderStatus(@PathVariable("id") Long id);
}

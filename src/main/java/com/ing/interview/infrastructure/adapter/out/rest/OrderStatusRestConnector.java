package com.ing.interview.infrastructure.adapter.out.rest;

import com.ing.interview.domain.command.query.order.GetOrderStatusCommad;
import com.ing.interview.domain.dipatcher.CommandQueryHandler;
import com.ing.interview.domain.model.OrderStatus;
import com.ing.interview.infrastructure.adapter.out.rest.client.OrderStatusRestClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class OrderStatusRestConnector implements CommandQueryHandler<GetOrderStatusCommad,OrderStatus> {

    private final OrderStatusRestClient orderStatusRestClient;

    @Override
    public OrderStatus commanQueryEvent(GetOrderStatusCommad event) {
        return orderStatusRestClient.checkOrderStatus(event.getId());
    }
}

package com.ing.interview.infrastructure.adapter.out.rest;

import com.ing.interview.domain.command.query.car.IsAvailableCarCommand;
import com.ing.interview.domain.dipatcher.CommandQueryHandler;
import com.ing.interview.infrastructure.adapter.out.rest.client.CarAvailabilityRestClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class CarAvailabilityRestConnector implements CommandQueryHandler<IsAvailableCarCommand,Boolean> {

    private final CarAvailabilityRestClient carAvailabilityRestClient;

    @Override
    public Boolean commanQueryEvent(IsAvailableCarCommand event) {
        return carAvailabilityRestClient.available(event.getColor(),event.getModel());
    }
}

package com.ing.interview.infrastructure.mapper;

import com.ing.interview.domain.command.car.CarCreateCommand;
import com.ing.interview.domain.model.Car;
import com.ing.interview.infrastructure.adapter.out.sql.entity.CarEntity;
import com.ing.interview.infrastructure.config.MapStructConfig;
import org.mapstruct.Mapper;


@Mapper(componentModel = "spring", config = MapStructConfig.class)
public abstract class MapperAdapter {

    public abstract CarEntity toCarEntity(CarCreateCommand carCreateCommand);

    public abstract Car toCar(CarEntity carEntity);
}
